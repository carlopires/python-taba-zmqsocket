"""
@author: Carlo Pires <carlopires@gmail.com>
"""
import os
import zmq


def _get_file_max():
    """
    Maybe should I get this from /proc/<PID>/limits ?
    """
    with open('/proc/sys/fs/file-max') as fhandle:
        return int(fhandle.read())

ctx = {}


def create_context():
    global ctx

    pid = os.getpid()

    if pid not in ctx:
        ctx[pid] = zmq.Context()

        # set MAX_SOCKETS to 50% of max files open
        # because a zmq socket may use more than
        # one file handle.
        ctx[pid].MAX_SOCKETS = _get_file_max()//2

    return ctx[pid]


def reset_context():
    global ctx
    pid = os.getpid()

    if pid in ctx:
        ctx[pid].term()
        del ctx[pid]


class SocketBase(object):
    def __init__(self, socket_type, addr):
        self.ctx = create_context()
        self.type = socket_type
        self.addr = addr
        self.socket = None

    def create_socket(self):
        self.close()
        self.socket = self.ctx.socket(self.type)
        self.open_socket()

    def open_socket(self):
        raise NotImplementedError()

    def ensure_valid_socket(self):
        if not self.socket or self.socket.closed:
            self.create_socket()

    def close(self):
        if self.socket and not self.socket.closed:
            self.socket.close(0)
            self.socket = None

    def send(self, *args, **kwargs):
        self.ensure_valid_socket()
        self.socket.send(*args, **kwargs)
        return True

    def recv(self, *args, **kwargs):
        self.ensure_valid_socket()
        return self.socket.recv(*args, **kwargs)


class SocketREP(SocketBase):
    def __init__(self, addr):
        super(SocketREP, self).__init__(zmq.REP, addr)

    def open_socket(self):
        self.socket.bind(self.addr)


class SocketREQ(SocketBase):
    def __init__(self, addr):
        super(SocketREQ, self).__init__(zmq.REQ, addr)

    def open_socket(self):
        self.socket.connect(self.addr)


class SocketPULL(SocketBase):
    def __init__(self, addr):
        super(SocketPULL, self).__init__(zmq.PULL, addr)

    def open_socket(self):
        self.socket.connect(self.addr)


class SocketPUSH(SocketBase):
    def __init__(self, addr):
        super(SocketPUSH, self).__init__(zmq.PUSH, addr)

    def open_socket(self):
        self.socket.bind(self.addr)


