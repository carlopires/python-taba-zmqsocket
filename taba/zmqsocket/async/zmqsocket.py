"""
@author: Carlo Pires <carlopires@gmail.com>
"""
import time
from asyncio import coroutine

import zmq
from ..zmqsocket import create_context  # @UnusedImport


class SocketBase(object):
    def __init__(self, loop, socket_type, addr):
        self.loop = loop
        self.ctx = create_context()
        self.type = socket_type
        self.addr = addr
        self.socket = None
        self.is_closing = False
        self.is_sender_active = False
        self.is_receiver_active = False

    def create_socket(self):
        assert self.socket is None
        self.socket = self.ctx.socket(self.type)
        self.open_socket()
        self.register_pollers()

    def register_pollers(self):
        if self.type in (zmq.REQ, zmq.REP, zmq.PULL):
            self.poller_in = zmq.Poller()
            self.poller_in.register(self.socket, zmq.POLLIN)

        if self.type in (zmq.REQ, zmq.REP, zmq.PUSH):
            self.poller_out = zmq.Poller()
            self.poller_out.register(self.socket, zmq.POLLOUT)

    def open_socket(self):
        raise NotImplementedError()

    def ensure_valid_socket(self):
        if self.socket is None:
            self.create_socket()
        elif self.socket.closed:
            if self.type in (zmq.REQ, zmq.REP, zmq.PULL):
                self.poller_in.unregister(self.socket)

            if self.type in (zmq.REQ, zmq.REP, zmq.PUSH):
                self.poller_out.unregister(self.socket)

            self.socket = None
            self.create_socket()

    def ensure_noblock(self, kwargs):
        kwargs['flags'] = kwargs.get('flags', 0) | zmq.NOBLOCK

    def ensure_dontwait(self, kwargs):
        kwargs['flags'] = kwargs.get('flags', 0) | zmq.DONTWAIT

    @coroutine
    def close(self):
        assert self.socket is not None

        self.is_closing = True
        while self._is_sender_active or self._is_receiver_active:
            yield

        yield from self._close()

    @coroutine
    def _close(self):
        if self.type in (zmq.REQ, zmq.REP, zmq.PULL):
            self.poller_in.unregister(self.socket)
        if self.type in (zmq.REQ, zmq.REP, zmq.PUSH):
            self.poller_out.unregister(self.socket)

        self.socket.close(0)
        self.socket = None

    @coroutine
    def send(self, *args, **kwargs):
        self._is_sender_active = True
        try:
            add_delay = False
            while 1:
                self.ensure_valid_socket()
                self.ensure_dontwait(kwargs)

                timeout = 10 if add_delay else 0
                polled = self.poller_out.poll(timeout)

                for socket, event in polled:
                    if event == zmq.POLLOUT:
                        socket.send(*args, **kwargs)
                        return True
                else:
                    t0 = time.monotonic()
                    yield
                    add_delay = time.monotonic() - t0 < 0.001

                if self.is_closing:
                    break
        finally:
            self._is_sender_active = False

    @coroutine
    def recv(self, timeout=None, **kwargs):
        if timeout:
            recv_start = time.monotonic()

        self._is_receiver_active = True
        try:
            add_delay = False
            while 1:
                self.ensure_valid_socket()
                self.ensure_noblock(kwargs)

                poll_timeout = 10 if add_delay else 0
                polled = self.poller_in.poll(poll_timeout)

                for socket, event in polled:
                    if event == zmq.POLLIN:
                        return socket.recv(**kwargs)

                t0 = time.monotonic()
                yield
                t1 = time.monotonic()
                add_delay = t1 - t0 < 0.001

                if timeout and t1 - recv_start > timeout:
                    break

                if self.is_closing:
                    break

        finally:
            self._is_receiver_active = False


class SocketREP(SocketBase):
    def __init__(self, loop, addr):
        super(SocketREP, self).__init__(loop, zmq.REP, addr)

    def open_socket(self):
        self.socket.bind(self.addr)


class SocketREQ(SocketBase):
    def __init__(self, loop, addr):
        super(SocketREQ, self).__init__(loop, zmq.REQ, addr)

    def open_socket(self):
        self.socket.connect(self.addr)


class SocketPULL(SocketBase):
    def __init__(self, loop, addr):
        super(SocketPULL, self).__init__(loop, zmq.PULL, addr)

    def open_socket(self):
        self.socket.connect(self.addr)


class SocketPUSH(SocketBase):
    def __init__(self, loop, addr):
        super(SocketPUSH, self).__init__(loop, zmq.PUSH, addr)

    def open_socket(self):
        self.socket.bind(self.addr)
