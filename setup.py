"""
@author: Carlo Pires <carlopires@gmail.com>
"""
from setuptools import setup

setup(
    name='taba.zmqsocket',
    version='14.09.1',
    url='https://taba.eco.br/pypi/taba-zmqsocket',
    author='Carlo Pires',
    author_email='carlopires@aldeiaglobal.com.br',
    description='Python interface for ZeroMQ sockets',
    namespace_packages=['taba'],
    packages=['taba', 'taba.zmqsocket', 'taba.zmqsocket.async'],
    zip_safe=True,
    platforms='any',
    install_requires=[
        'pyzmq>=14.3.1',
    ],
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        'Intended Audience :: Developers',
        "License :: Taba Platform License",
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        "Framework :: Taba",
        'Topic :: Software Development :: Libraries :: Python Modules'
    ],
    test_suite='tests.suite',
)
