# python-taba-zmqsocket #

Socket interface for ZeroMQ sockets. This package is intended to be used with Python3.x.

### Installation ###

    pip3 install git+https://carlopires@bitbucket.org/carlopires/python-taba-zmqsocket.git

### Sync version ###

```
#!python

from taba.zmqsocket import zmqsocket
ctx = zmqsocket.create_context()
s = zmqsocket.SocketREP(ctx, 'tcp://127.0.0.1:12345')
data = s.recv()
s.send(data)
```

#### Async version ####


```
#!python

from asyncio import coroutine
from taba.zmqsocket.async import zmqsocket

@coroutine
def server(loop)
    ctx = zmqsocket.create_context()
    s = zmqsocket.SocketREP(loop, ctx, 'tcp://127.0.0.1:10106')
    data = (yield from s.recv())
    yield from s.send(data)

loop = get_event_loop()
loop.run_until_complete(server(loop))

```

