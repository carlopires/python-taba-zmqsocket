import unittest

def suite():
    from . import test_zmqsocket
    from . import test_zmqsocket_async
    suite = unittest.TestSuite()
    suite.addTest(test_zmqsocket.suite())
    suite.addTest(test_zmqsocket_async.suite())
    return suite

if __name__ == '__main__':
    unittest.TextTestRunner(verbosity=2).run(suite())