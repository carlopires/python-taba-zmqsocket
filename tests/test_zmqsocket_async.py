"""
@author: Carlo Pires <carlopires@gmail.com>
"""
import unittest
from asyncio import coroutine, get_event_loop, async, gather
from taba.zmqsocket.async import zmqsocket

@coroutine
def server(loop, ctx, test):
    s = zmqsocket.SocketREP(loop, ctx, 'tcp://127.0.0.1:10106')
    test.assertEqual((yield from s.recv()), b'1')
    test.assertEqual((yield from s.send(b'OK')), True)
    test.assertEqual((yield from s.recv()), b'2')
    test.assertEqual((yield from s.send(b'OK')), True)
    test.assertEqual((yield from s.recv()), b'3')
    test.assertEqual((yield from s.send(b'OK')), True)
    yield from s.close()

@coroutine
def client(loop, ctx, test):
    s = zmqsocket.SocketREQ(loop, ctx, 'tcp://127.0.0.1:10106')
    test.assertEqual((yield from s.send(b'1')), True)
    test.assertEqual((yield from s.recv()), b'OK')
    test.assertEqual((yield from s.send(b'2')), True)
    test.assertEqual((yield from s.recv()), b'OK')
    test.assertEqual((yield from s.send(b'3')), True)
    test.assertEqual((yield from s.recv()), b'OK')
    yield from s.close()

@coroutine
def tests(loop, test):
    with open('/proc/sys/fs/file-max') as fhandle:
        fs_max = int(fhandle.read())
    
    ctx = zmqsocket.create_context()
    test.assertEqual(ctx.MAX_SOCKETS, fs_max//2)
    
    srv = async(server(loop, ctx, test), loop=loop)
    cli = async(client(loop, ctx, test), loop=loop)
    
    g = gather(srv, cli)
    while 1:
        yield
        if g.done():
            break
    
    test.assertEqual(srv.done(), True)
    test.assertEqual(cli.done(), True)
    test.assertEqual(srv.result(), None)
    test.assertEqual(cli.result(), None)

class ZmqSocketAsyncREQREPTest(unittest.TestCase):
    def test_client_server_async(self):
        loop = get_event_loop()
        loop.run_until_complete(tests(loop, self))

def suite():
    loader = unittest.TestLoader()
    suite = unittest.TestSuite()
    suite.addTest(loader.loadTestsFromTestCase(ZmqSocketAsyncREQREPTest))
    return suite

