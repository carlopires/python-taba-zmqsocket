"""
@author: Carlo Pires <carlopires@gmail.com>
"""
import unittest, threading
from taba.zmqsocket import zmqsocket

class server(threading.Thread):
    def __init__(self, ctx, test):
        super(server, self).__init__()
        self.ctx = ctx
        self.test = test
    
    def run(self):
        s = zmqsocket.SocketREP(self.ctx, 'tcp://127.0.0.1:19105')
        self.test.assertEqual(s.recv(), b'1')
        self.test.assertEqual(s.send(b'OK'), True)
        self.test.assertEqual(s.recv(), b'2')
        self.test.assertEqual(s.send(b'OK'), True)
        self.test.assertEqual(s.recv(), b'3')
        self.test.assertEqual(s.send(b'OK'), True)
        s.close()

class client(threading.Thread):
    def __init__(self, ctx, test):
        super(client, self).__init__()
        self.ctx = ctx
        self.test = test
        
    def run(self):
        s = zmqsocket.SocketREQ(self.ctx, 'tcp://127.0.0.1:19105')
        self.test.assertEqual(s.send(b'1'), True)
        self.test.assertEqual(s.recv(), b'OK')
        self.test.assertEqual(s.send(b'2'), True)
        self.test.assertEqual(s.recv(), b'OK')
        self.test.assertEqual(s.send(b'3'), True)
        self.test.assertEqual(s.recv(), b'OK')
        s.close()

class ZmqSocketREQREPTest(unittest.TestCase):
    def test_client_server(self):
        with open('/proc/sys/fs/file-max') as fhandle:
            fs_max = int(fhandle.read())
        
        ctx = zmqsocket.create_context()
        self.assertEqual(ctx.MAX_SOCKETS, fs_max//2)
        s = server(ctx, self)
        c = client(ctx, self)
        s.start()
        c.start()
        c.join()
        s.join()

def suite():
    loader = unittest.TestLoader()
    suite = unittest.TestSuite()
    suite.addTest(loader.loadTestsFromTestCase(ZmqSocketREQREPTest))
    return suite
